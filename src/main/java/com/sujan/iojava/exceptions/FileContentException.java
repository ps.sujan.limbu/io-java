package com.sujan.iojava.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class FileContentException extends RuntimeException {
    public FileContentException(String message) {
        super(message);
    }
}
