package com.sujan.iojava.exceptions;

public class CorsException extends RuntimeException {
    public CorsException(String message) {
        super(message);
    }
}
