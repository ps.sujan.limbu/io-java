package com.sujan.iojava.exceptions;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomizedGlobalExceptionManager extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(CustomizedGlobalExceptionManager.class);

    @ExceptionHandler(value = {FileContentException.class, TokenException.class,
            InvalidFileFormatException.class, CorsException.class})
    public ResponseEntity<ExceptionResponse> handleBadRequestException(Exception exception){
        final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareExceptionResponse(exception, httpStatus),httpStatus);
    }

    @ExceptionHandler(value = {FileNotFoundException.class, BadCredentialsException.class})
    public ResponseEntity<ExceptionResponse> handleNotFoundException(Exception exception){
        final HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(prepareExceptionResponse(exception, httpStatus),httpStatus);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> handleOtherException(Exception exception){
        final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(prepareExceptionResponse(exception, httpStatus),httpStatus);
    }

    @ExceptionHandler(value = {AccessDeniedException.class})
    public ResponseEntity<ExceptionResponse> handleAccessDeniedException(AccessDeniedException exception){
        final HttpStatus httpStatus = HttpStatus.FORBIDDEN;
        return new ResponseEntity<>(prepareExceptionResponse(exception, httpStatus),httpStatus);
    }

    @NotNull
    private ExceptionResponse prepareExceptionResponse(Exception exception, HttpStatus httpStatus) {
        logger.info(exception.toString());
        ExceptionResponse exceptionResponse = new ExceptionResponse();
        exceptionResponse.setDateTime(LocalDateTime.now());
        exceptionResponse.setStatusCode(httpStatus.value());
        exceptionResponse.setMessage(exception.getMessage());
        return exceptionResponse;
    }

}
