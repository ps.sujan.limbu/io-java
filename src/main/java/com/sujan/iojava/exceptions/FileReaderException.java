package com.sujan.iojava.exceptions;

public class FileReaderException extends RuntimeException {
    public FileReaderException(String message) {
        super(message);
    }
}
