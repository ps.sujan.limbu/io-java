package com.sujan.iojava.exceptions;

public class FileWriterException extends RuntimeException {
    public FileWriterException(String message) {
        super(message);
    }
}
