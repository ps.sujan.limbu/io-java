package com.sujan.iojava;

import com.sujan.iojava.model.TransactionInfo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface FileHandler {
    List<TransactionInfo> read(InputStream inputStream);
    String write(String[] data, String dest);

}
