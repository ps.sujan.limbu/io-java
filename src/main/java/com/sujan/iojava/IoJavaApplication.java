package com.sujan.iojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class IoJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IoJavaApplication.class, args);
	}

}
