package com.sujan.iojava.enums;

public enum CurrencyCode {
    JOD("Jordan Dinar",3),
    USD("United States Dollar",2);

    private String description;
    private int decimalSeparator;

    CurrencyCode(String description, int decimalSeparator) {
        this.description = description;
        this.decimalSeparator = decimalSeparator;
    }

    public String getDescription() {
        return description;
    }

    public int getDecimalSeparator() {
        return decimalSeparator;
    }
}