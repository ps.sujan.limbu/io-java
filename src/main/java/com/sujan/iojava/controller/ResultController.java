package com.sujan.iojava.controller;

import com.sujan.iojava.services.CSVHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/result")
public class ResultController {
    private final CSVHandler csvHandler;

    @Value("${result.directory}")
    private String resultDir;

    @Value("${result.matching.transactions.name}")
    private String matchingDir;

    @Value("${result.mismatching.transactions.name}")
    private String misMatchingDir;

    @Value("${result.missing.transactions.name}")
    private String missingDir;

    public ResultController(CSVHandler csvHandler) {
        this.csvHandler = csvHandler;
    }

    @GetMapping("/matching")
    public ResponseEntity<Object> getMatchingTransactions() {
        List<String> response = csvHandler.read(resultDir.concat(matchingDir));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/mis-matching")
    public ResponseEntity<Object> getMisMatchingTransactions() {
        List<String> response = csvHandler.read(resultDir.concat(misMatchingDir));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/missing")
    public ResponseEntity<Object> getMissingTransactions() {
        List<String> response = csvHandler.read(resultDir.concat(missingDir));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
