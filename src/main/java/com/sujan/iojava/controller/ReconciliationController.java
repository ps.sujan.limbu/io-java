package com.sujan.iojava.controller;

import com.sujan.iojava.model.TransactionInfo;
import com.sujan.iojava.services.ReconciliationService;
import com.sujan.iojava.util.MultipartFileHelper;
import com.sujan.iojava.util.ResultLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/v1/reconciliation")
public class ReconciliationController {
    private final ReconciliationService reconciliationService;
    private final MultipartFileHelper fileHelper;

    public ReconciliationController(ReconciliationService reconciliationService, MultipartFileHelper fileHelper) {
        this.reconciliationService = reconciliationService;
        this.fileHelper = fileHelper;
    }

    @PostMapping("/import/files")
    public ResponseEntity<Object> generateResult(@RequestBody MultipartFile sourceFile,@RequestBody MultipartFile targetFile) {
        List<TransactionInfo> sourceFileInfo = fileHelper.readDataFromFile(sourceFile);
        List<TransactionInfo> targetFileInfo = fileHelper.readDataFromFile(targetFile);
        reconciliationService.executeReconciliation(sourceFileInfo, targetFileInfo);
        return new ResponseEntity<>(ResultLinkBuilder.getReconciliationResults(), HttpStatus.OK);
    }

}
