package com.sujan.iojava.services;

import com.sujan.iojava.FileHandler;
import com.sujan.iojava.enums.CurrencyCode;
import com.sujan.iojava.exceptions.FileNotFoundException;
import com.sujan.iojava.exceptions.FileReaderException;
import com.sujan.iojava.exceptions.FileWriterException;
import com.sujan.iojava.model.TransactionInfo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Qualifier("csvHandler")
public class CSVHandler implements FileHandler {

    @Override
    public List<TransactionInfo> read(InputStream inputStream) {
        List<TransactionInfo> dataRead = new ArrayList<>();
        BufferedReader csvReader = new BufferedReader(new InputStreamReader(inputStream));
        String nextLine = null;
        try {
            nextLine = csvReader.readLine();
            while ((nextLine = csvReader.readLine())!=null) {
                String[] dataInEachLine = nextLine.split(",");
                dataRead.add(new TransactionInfo(dataInEachLine[0],dataInEachLine[1],Double.parseDouble(dataInEachLine[2])
                        , CurrencyCode.valueOf(dataInEachLine[3]),dataInEachLine[4], dataInEachLine[5],dataInEachLine[6]));
            }
            csvReader.close();
        } catch (Exception e) {
            throw new FileReaderException("Something went wrong reading the CSV file");
        }
        return dataRead;
    }

    @Override
    public String write(String[] data, String dest) {
        File file = new File(dest);
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            for (String datum : data) {
                bufferedWriter.write(datum);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            bufferedWriter.close();
        } catch (Exception e) {
            throw new FileWriterException("Something went wrong writing the CSV file");
        }
        return file.getParent().concat("\\").concat(file.getName());
    }

    public List<String> read(String path) {
        if (path !=null) {
            try {
                return new BufferedReader(new FileReader(path)).lines().collect(Collectors.toList());
            } catch (IOException e) {
                throw new FileNotFoundException("File not found in: "+path);
            }
        }
        return new ArrayList<>();
    }
}
