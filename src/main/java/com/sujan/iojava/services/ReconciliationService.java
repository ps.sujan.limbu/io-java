package com.sujan.iojava.services;

import com.sujan.iojava.enums.FileType;
import com.sujan.iojava.model.MatchingTransactionsInfo;
import com.sujan.iojava.model.MisMatchingTransactionsInfo;
import com.sujan.iojava.model.MissingTransactionsInfo;
import com.sujan.iojava.model.TransactionInfo;
import com.sujan.iojava.util.FileUtils;
import com.sujan.iojava.util.MathsUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReconciliationService {
    @Value("${result.directory}")
    private String resultDir;

    @Value("${result.matching.transactions.name}")
    private String matchingDir;

    @Value("${result.mismatching.transactions.name}")
    private String misMatchingDir;

    @Value("${result.missing.transactions.name}")
    private String missingDir;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReconciliationService.class);

    public String executeReconciliation(List<TransactionInfo> sourceData, List<TransactionInfo> targetData) {
        List<MatchingTransactionsInfo> matchingTransactionsInfoList = new ArrayList<>();
        List<MisMatchingTransactionsInfo> misMatchingTransactionsInfoList = new ArrayList<>();
        List<MissingTransactionsInfo> missingTransactionsInfoList = new ArrayList<>();

        filterMatchingAndMisMatchingTransaction(sourceData, targetData, matchingTransactionsInfoList, misMatchingTransactionsInfoList);
        filterMissingTransactions(sourceData, targetData, missingTransactionsInfoList);

        String[] matchingData = new String[matchingTransactionsInfoList.size()+1];
        matchingData[0] = "transaction id,amount,currency code,value date";
        prepareMatchedTransactionsToWrite(matchingTransactionsInfoList, matchingData);

        String[] mismatchedData = new String[misMatchingTransactionsInfoList.size()+1];
        mismatchedData[0] = "found in file,transaction id,amount,currency code,value date";
        prepareMisMatchedTransactionToWrite(misMatchingTransactionsInfoList, mismatchedData);

        String[] missingData = new String[missingTransactionsInfoList.size()+1];
        missingData[0] = "found in file,transaction id,amount,currency code,value date";
        prepareMissingTransactionToWrite(missingTransactionsInfoList, missingData);

        return writeResultToCsv(matchingData, mismatchedData, missingData);
    }

    private String writeResultToCsv(String[] matchingData, String[] mismatchedData, String[] missingData) {
        FileUtils.createDirectory(resultDir);
        CSVHandler csvHandler = new CSVHandler();
        csvHandler.write(matchingData, resultDir.concat(matchingDir));
        csvHandler.write(mismatchedData, resultDir.concat(misMatchingDir));
        csvHandler.write(missingData, resultDir.concat(missingDir));
        return new File(resultDir).getAbsolutePath();
    }

    private void prepareMatchedTransactionsToWrite(List<MatchingTransactionsInfo> matchingTransactionsInfoList, String[] matchingData) {
        for (int i = 0; i < (matchingData.length-1); i++) {
            matchingData[i+1]=matchingTransactionsInfoList.get(i).getTransactionId()
                    +", "+ MathsUtil.round(matchingTransactionsInfoList.get(i).getAmount(), matchingTransactionsInfoList.get(i).getCurrencyCode().getDecimalSeparator())
                    +", "+matchingTransactionsInfoList.get(i).getCurrencyCode()
                    +", "+matchingTransactionsInfoList.get(i).getValueDate();
        }
    }

    private void prepareMissingTransactionToWrite(List<MissingTransactionsInfo> missingTransactionsInfoList, String[] missingData) {
        for (int i = 0; i < (missingData.length-1); i++) {
            missingData[i+1]=missingTransactionsInfoList.get(i).getFoundInFile()
                    +", "+missingTransactionsInfoList.get(i).getTransactionId()
                    +", "+ MathsUtil.round(missingTransactionsInfoList.get(i).getAmount(), missingTransactionsInfoList.get(i).getCurrencyCode().getDecimalSeparator())
                    +", "+missingTransactionsInfoList.get(i).getCurrencyCode()
                    +", "+missingTransactionsInfoList.get(i).getValueDate();
        }
    }

    private void prepareMisMatchedTransactionToWrite(List<MisMatchingTransactionsInfo> misMatchingTransactionsInfoList, String[] mismatchedData) {
        for (int i = 0; i < (mismatchedData.length-1); i++) {
            mismatchedData[i+1]=misMatchingTransactionsInfoList.get(i).getFoundInFile()
                    +", "+misMatchingTransactionsInfoList.get(i).getTransactionId()
                    +", "+ MathsUtil.round(misMatchingTransactionsInfoList.get(i).getAmount(), misMatchingTransactionsInfoList.get(i).getCurrencyCode().getDecimalSeparator())
                    +", "+misMatchingTransactionsInfoList.get(i).getCurrencyCode()
                    +", "+misMatchingTransactionsInfoList.get(i).getValueDate();
        }
    }

    private void filterMissingTransactions(List<TransactionInfo> sourceData, List<TransactionInfo> targetData, List<MissingTransactionsInfo> missingTransactionsInfoList) {
        List<TransactionInfo> tempMissingTransactionList = sourceData.stream().filter(
                src -> targetData.stream().noneMatch(target->
                        target.getTransactionId().equalsIgnoreCase(src.getTransactionId())))
                .collect(Collectors.toList());
        tempMissingTransactionList.forEach(srcData-> {
            missingTransactionsInfoList.add(new MissingTransactionsInfo(FileType.SOURCE,
                    srcData.getTransactionId(), srcData.getAmount(), srcData.getCurrencyCode(), srcData.getValueDate()));
        });
        tempMissingTransactionList.clear();
        tempMissingTransactionList.addAll(targetData.stream().filter(
                target -> sourceData.stream().noneMatch(src->
                        target.getTransactionId().equalsIgnoreCase(src.getTransactionId())))
                .collect(Collectors.toList()));
        tempMissingTransactionList.forEach(trgData->{
            missingTransactionsInfoList.add(new MissingTransactionsInfo(FileType.TARGET,
                    trgData.getTransactionId(), trgData.getAmount(), trgData.getCurrencyCode(), trgData.getValueDate()));
        });
    }

    private void filterMatchingAndMisMatchingTransaction(List<TransactionInfo> sourceData, List<TransactionInfo> targetData, List<MatchingTransactionsInfo> matchingTransactionsInfoList, List<MisMatchingTransactionsInfo> misMatchingTransactionsInfoList) {
        sourceData.forEach(eachSrcData->{
            targetData.stream()
                    .filter(eachTargetData -> eachSrcData.getTransactionId().equalsIgnoreCase(eachTargetData.getTransactionId()))
                    .forEach(sameIdTarget-> {
                        if (eachSrcData.getAmount().compareTo(sameIdTarget.getAmount()) == 0
                                && eachSrcData.getCurrencyCode().equals(sameIdTarget.getCurrencyCode())
                                && eachSrcData.getValueDate().equals(sameIdTarget.getValueDate())) {
                            matchingTransactionsInfoList.add(new MatchingTransactionsInfo(eachSrcData.getTransactionId(), eachSrcData.getAmount(), eachSrcData.getCurrencyCode(), eachSrcData.getValueDate()));
                        } else {
                            //MisMatched
                            misMatchingTransactionsInfoList.add(new MisMatchingTransactionsInfo(FileType.SOURCE, eachSrcData.getTransactionId(),
                                    eachSrcData.getAmount(),eachSrcData.getCurrencyCode() ,eachSrcData.getValueDate()));
                            misMatchingTransactionsInfoList.add(new MisMatchingTransactionsInfo(FileType.TARGET, sameIdTarget.getTransactionId(),
                                    sameIdTarget.getAmount(),sameIdTarget.getCurrencyCode() ,sameIdTarget.getValueDate()));
                        }
                    });
        });
    }

}
