package com.sujan.iojava.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sujan.iojava.FileHandler;
import com.sujan.iojava.exceptions.FileReaderException;
import com.sujan.iojava.exceptions.ParsingException;
import com.sujan.iojava.model.JsonTransactionInfo;
import com.sujan.iojava.model.TransactionInfo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Qualifier("jsonHandler")
public class JSONHandler implements FileHandler{

    @Override
    public List<TransactionInfo> read(InputStream inputStream) {
        List<JsonTransactionInfo> dataFromJson;
        try{
            dataFromJson = new Gson().fromJson(new BufferedReader(new InputStreamReader(inputStream)), new TypeToken<List<JsonTransactionInfo>>() {}.getType());
            dataFromJson.forEach(data -> {
                data.setDate(parseDateToProperFormat(data.getDate()));
            });
        }catch (Exception e){
            throw new FileReaderException("Something went wrong reading the json file");
        }
        return convertToTransactionInfo(dataFromJson);
    }
    @Override
    public String write(String[] data, String dest) {
        return "";
    }

    private String parseDateToProperFormat(String date) {
        DateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parsedDate = null;
        try {
            parsedDate = originalFormat.parse(date);
        } catch (ParseException e) {
            throw new ParsingException("Error in date conversion in the json file");
        }
        return targetFormat.format(parsedDate);
    }

    private List<TransactionInfo> convertToTransactionInfo(List<JsonTransactionInfo> fromJson) {
        List<TransactionInfo> transactionInfoList = new ArrayList<>();
        fromJson.forEach(jsonData -> transactionInfoList.add(new TransactionInfo(jsonData.getReference()," ", jsonData.getAmount(), jsonData.getCurrencyCode(),jsonData.getPurpose(),jsonData.getDate()," ")));
        return transactionInfoList;
    }


}
