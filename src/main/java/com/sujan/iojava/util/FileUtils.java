package com.sujan.iojava.util;

import java.io.File;

public class FileUtils {

    public static void createDirectory(String path){
        File file = null;
        if (path!=null)
            file = new File(path);
        if (!file.exists())
            file.mkdir();
    }
}
