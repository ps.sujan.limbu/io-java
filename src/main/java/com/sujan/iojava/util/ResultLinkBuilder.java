package com.sujan.iojava.util;

import com.sujan.iojava.controller.ResultController;
import com.sujan.iojava.model.ReconciliationResult;
import org.springframework.hateoas.Link;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class ResultLinkBuilder {

    public static  List<ReconciliationResult> getReconciliationResults() {
        Link matchedLink = linkTo(methodOn(ResultController.class).getMatchingTransactions()).withRel("matching");
        Link misMatchedLink = linkTo(methodOn(ResultController.class).getMisMatchingTransactions()).withRel("mis-matching");
        Link missingLink = linkTo(methodOn(ResultController.class).getMissingTransactions()).withRel("missing");
        return List.of(new ReconciliationResult(matchedLink,"matched.csv"),
                new ReconciliationResult(misMatchedLink,"mis-matched.csv"),
                new ReconciliationResult(missingLink,"missing.csv"));
    }
}
