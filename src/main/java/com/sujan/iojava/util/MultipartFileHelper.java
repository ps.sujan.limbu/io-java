package com.sujan.iojava.util;

import com.sujan.iojava.FileHandler;
import com.sujan.iojava.exceptions.FileContentException;
import com.sujan.iojava.exceptions.FileReaderException;
import com.sujan.iojava.model.TransactionInfo;
import com.sujan.iojava.exceptions.InvalidFileFormatException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MultipartFileHelper {
    private final FileHandler csvHandler;
    private final FileHandler jsonHandler;

    public MultipartFileHelper(@Qualifier("csvHandler")FileHandler csvHandler, @Qualifier("jsonHandler")FileHandler jsonHandler) {
        this.csvHandler = csvHandler;
        this.jsonHandler = jsonHandler;
    }

    public List<TransactionInfo> readDataFromFile(MultipartFile file) {
        List<TransactionInfo> transactionInfoList = new ArrayList<>();
        if (file==null||file.getContentType() == null)
            throw new FileContentException("Error in File Content.");
        try{
            if (file.getContentType().equalsIgnoreCase("text/csv")|| file.getContentType().equalsIgnoreCase("application/vnd.ms-excel"))
                transactionInfoList = csvHandler.read(file.getInputStream());
            else  if (file.getContentType().equalsIgnoreCase("application/json"))
                transactionInfoList = jsonHandler.read(file.getInputStream());
            else
                throw new InvalidFileFormatException("Source/Target File format seems to be invalid." +
                        "Only CSV and JSON are supported for now.");
        }catch (IOException e){
            throw new FileReaderException("Error in reading files.");
        }

        return transactionInfoList;
    }

}
