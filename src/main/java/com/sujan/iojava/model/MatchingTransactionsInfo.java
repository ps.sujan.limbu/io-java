package com.sujan.iojava.model;

import com.sujan.iojava.enums.CurrencyCode;

public class MatchingTransactionsInfo {
    private String transactionId;
    private Double amount;
    private CurrencyCode currencyCode;
    private String valueDate;

    public MatchingTransactionsInfo() {
    }

    public MatchingTransactionsInfo(String transactionId, Double amount, CurrencyCode currencyCode, String valueDate) {
        this.transactionId = transactionId;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.valueDate = valueDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    @Override
    public String toString() {
        return "MatchingTransactionsInfo{" +
                "transactionId='" + transactionId + '\'' +
                ", amount=" + amount +
                ", currencyCode='" + currencyCode + '\'' +
                ", valueDate='" + valueDate + '\'' +
                '}';
    }
}
