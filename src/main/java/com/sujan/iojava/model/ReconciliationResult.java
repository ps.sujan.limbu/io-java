package com.sujan.iojava.model;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

public class ReconciliationResult extends RepresentationModel<ReconciliationResult> {
    private String fileName;

    public ReconciliationResult(Link initialLink, String fileName) {
        super(initialLink);
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
