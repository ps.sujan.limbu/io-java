package com.sujan.iojava.model;

import com.sujan.iojava.enums.CurrencyCode;

public class JsonTransactionInfo {
    private String date;
    private String reference;
    private CurrencyCode currencyCode;
    private String purpose;
    private Double amount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "JsonTransaction{" +
                "date='" + date + '\'' +
                ", reference='" + reference + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", purpose='" + purpose + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}