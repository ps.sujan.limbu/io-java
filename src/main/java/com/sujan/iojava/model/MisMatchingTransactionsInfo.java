package com.sujan.iojava.model;

import com.sujan.iojava.enums.CurrencyCode;
import com.sujan.iojava.enums.FileType;

public class MisMatchingTransactionsInfo {
    private FileType foundInFile;
    private String transactionId;
    private Double amount;
    private CurrencyCode currencyCode;
    private String valueDate;

    public MisMatchingTransactionsInfo() {
    }

    public MisMatchingTransactionsInfo(FileType foundInFile, String transactionId, Double amount, CurrencyCode currencyCode, String valueDate) {
        this.foundInFile = foundInFile;
        this.transactionId = transactionId;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.valueDate = valueDate;
    }

    public FileType getFoundInFile() {
        return foundInFile;
    }

    public void setFoundInFile(FileType foundInFile) {
        this.foundInFile = foundInFile;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }
    @Override
    public String toString() {
        return "MisMatchingTransactionsInfo{" +
                "foundInFile=" + foundInFile +
                ", transactionId='" + transactionId + '\'' +
                ", amount=" + amount +
                ", currencyCode='" + currencyCode + '\'' +
                ", valueDate='" + valueDate + '\'' +
                '}';
    }
}
