package com.sujan.iojava.model;

import com.sujan.iojava.enums.CurrencyCode;

public class TransactionInfo {
    private String transactionId;
    private String transactionDescription;
    private Double amount;
    private CurrencyCode currencyCode;
    private String purpose;
    private String valueDate;
    private String transactionType;

    public TransactionInfo() {
    }

    public TransactionInfo(String transactionId, String transactionDescription, Double amount, CurrencyCode currencyCode, String purpose, String valueDate, String transactionType) {
        this.transactionId = transactionId;
        this.transactionDescription = transactionDescription;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.purpose = purpose;
        this.valueDate = valueDate;
        this.transactionType = transactionType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    @Override
    public String toString() {
        return "TransactionInfo{" +
                "transactionId='" + transactionId + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", amount=" + amount +
                ", currencyCode='" + currencyCode + '\'' +
                ", valueDate='" + valueDate + '\'' +
                ", purpose='" + purpose + '\'' +
                '}';
    }
}
