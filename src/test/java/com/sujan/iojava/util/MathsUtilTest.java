package com.sujan.iojava.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathsUtilTest {
    @Test
    void shouldThrowIllegalArgumentException_whenDecimalValueIsLessThan0(){
        Assertions.assertThrows(IllegalArgumentException.class, ()-> MathsUtil.round(12.056789,-1));
    }
    @Test
    void whenGivenDoubleValueWith4DecimalValue_mustReturnDoubleValueWith2Decimal(){
        assertEquals(12.06, MathsUtil.round(12.05678, 2));
    }

    @Test
    void whenGivenDoubleValueWith1DecimalValue_mustReturnDoubleValueWith2Decimal(){
        assertEquals(12.1, MathsUtil.round(12.05678, 1));
    }

}