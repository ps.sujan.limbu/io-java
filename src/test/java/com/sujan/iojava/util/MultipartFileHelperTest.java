package com.sujan.iojava.util;

import com.sujan.iojava.FileHandler;
import com.sujan.iojava.model.TransactionInfo;
import com.sujan.iojava.exceptions.InvalidFileFormatException;
import com.sujan.iojava.services.CSVHandler;
import com.sujan.iojava.services.JSONHandler;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.List;

class MultipartFileHelperTest {
    private MultipartFileHelper multipartFileHelper;
    private FileHandler csvHandler;
    private FileHandler jsonHandler;
    @BeforeEach
    void setUp(){
        csvHandler = new CSVHandler();
        jsonHandler = new JSONHandler();
        multipartFileHelper = new MultipartFileHelper(csvHandler, jsonHandler);
    }

    @Test
    void shouldThrowFileFormatNotSupportedException_whenFileWithInvalidFormatIsGiven() throws IOException {
        String path = "src\\test\\resources\\sample.txt";
        new FileWriter(new File(path)).write("testing");
        FileInputStream fis = new FileInputStream(path);
        MockMultipartFile multipartFile = new MockMultipartFile("file",
                "sample.txt",
                "text",
                fis);
        assertThrows(InvalidFileFormatException.class, ()-> multipartFileHelper.readDataFromFile(multipartFile));
    }

    @Test
    void testIfCSVFileIsRead() throws IOException {
        String path = "src\\test\\resources\\sample-files\\input-files\\bank-transactions.csv";
        FileInputStream fis = new FileInputStream(path);
        MockMultipartFile multipartFile = new MockMultipartFile("bank-transactions.csv",
                "bank-transactions.csv",
                "text/csv",
                fis);
        List<TransactionInfo> actualTx = multipartFileHelper.readDataFromFile(multipartFile);
        assertEquals(6 , actualTx.size());
        assertEquals("TR-47884222203" , actualTx.get(2).getTransactionId());
    }

    @Test
    void testIfJsonFileIsRead() throws IOException {
        String path = "src\\test\\resources\\sample-files\\input-files\\online-banking-transactions.json";
        FileInputStream fis = new FileInputStream(path);
        MockMultipartFile multipartFile = new MockMultipartFile("bank-transactions.csv",
                "online-banking-transactions.json",
                "application/json",
                fis);
        List<TransactionInfo> actualTx = multipartFileHelper.readDataFromFile(multipartFile);
        assertEquals(7 , actualTx.size());
        assertEquals("TR-47884222217" , actualTx.get(4).getTransactionId());
    }

    @AfterAll
    static void cleanUp(){
        String path = "src\\test\\resources\\sample.txt";
        File file = new File(path);
        if (file.exists())
            file.delete();
    }
}