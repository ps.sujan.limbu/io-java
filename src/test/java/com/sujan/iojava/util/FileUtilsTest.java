package com.sujan.iojava.util;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class FileUtilsTest {
    static final String testPath = "src\\main\\resources\\test-dir";

    @Test
    void testIfDirectoryIsCreated(){
        FileUtils.createDirectory(testPath);
        File file = new File(testPath);
        assertTrue(file.exists());
    }

    @AfterAll
    static void cleanUp(){
        File file = new File(testPath);
        if (file.exists())
            file.delete();
    }

}