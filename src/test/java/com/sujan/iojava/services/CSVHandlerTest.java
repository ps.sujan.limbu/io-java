package com.sujan.iojava.services;

import com.sujan.iojava.FileHandler;
import com.sujan.iojava.exceptions.FileReaderException;
import com.sujan.iojava.exceptions.FileWriterException;
import com.sujan.iojava.model.TransactionInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.mock.web.MockMultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CSVHandlerTest {
    @Autowired
    private CSVHandler csvHandler;
    @Value("${sample.csv.content.path}")
    private String csvPath;

    @Test
    void shouldThrowFileWriterException_whenInvalidDestinationIsGivenToSaveCSV(){
        String invalidDest = "invalid\\file.csv";
        assertThrows(FileWriterException.class, ()->csvHandler.write(new String[]{}, invalidDest));
    }

    @Test
    void checkIfFileIsWrittenInValidDirectory() {
        String fileName = "test-bank-transactions.csv";
        String dest = "src\\test\\resources\\"+fileName;
        String[] data = new String[]{"some data"};
        String actualPath = csvHandler.write(data, dest);
        System.out.println(actualPath);
        assertEquals("src\\test\\resources\\test-bank-transactions.csv", actualPath);
    }

    @Test
    void checkIfFileWrittenHasValidData() throws IOException {
        String fileName = "test-bank-transactions.csv";
        String dest = "src\\test\\resources\\"+fileName;
        String[] data = {"TR-47884222201,online transfer,140,USD,donation,2020-01-20,D",
                "TR-47884222202,atm withdrwal,20.0000,JOD,,2020-01-22,D",
                "TR-47884222203,counter withdrawal,5000,JOD,,2020-01-25,D",
                "TR-47884222204,salary,1200.000,JOD,donation,2020-01-31,C",
                "TR-47884222205,atm withdrwal,60.0,JOD,,2020-02-02,D",
                "TR-47884222206,atm withdrwal,500.0,USD,,2020-02-10,D"};
        csvHandler.write(data, dest);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(dest));
        List<String> actualData = bufferedReader.lines().collect(Collectors.toList());
        assertEquals(6, actualData.size());
        assertEquals("TR-47884222201,online transfer,140,USD,donation,2020-01-20,D", actualData.get(0));
        assertEquals(7, actualData.get(0).split(",").length);

        String[] index1ofActualData = {"TR-47884222201","online transfer","140","USD","donation","2020-01-20","D"};
        String[] index1ofExpectedData = actualData.get(0).split(",");
        assertEquals(index1ofExpectedData[0], index1ofActualData[0]);
        assertEquals(index1ofExpectedData[1], index1ofActualData[1]);
        assertEquals(index1ofExpectedData[2], index1ofActualData[2]);
        assertEquals(index1ofExpectedData[3], index1ofActualData[3]);
        assertEquals(index1ofExpectedData[4], index1ofActualData[4]);
    }
    @Test
    void shouldThrowFileReaderException_whenThereIsErrorInDataInputStream(){
        String erData ="trans unique id,trans description,amount,currecny,purpose,value date,trans type\nTR-47884222201,online transfer,invalid amount,USD,donation,2020-01-20,D";
        assertThrows(FileReaderException.class, ()->csvHandler.read(erData));
    }
    @Test
    void testIfDataOfGivenInputStreamReadIsCorrect() throws IOException {
        FileInputStream fis = new FileInputStream(csvPath);
        List<TransactionInfo> actualTxList = csvHandler.read(fis);
        assertEquals(6, actualTxList.size());
        assertEquals("TR-47884222203" , actualTxList.get(2).getTransactionId());
    }

    @Test
    void shouldThrowFileReaderException_whenInvalidCSVSourceIsGiven(){
        String invalidSrc = "invalid\\file.csv";
        assertThrows(FileReaderException.class, ()->csvHandler.read(invalidSrc));
    }
    @Test
    void shouldReturnEmptyList_whenCSVSourceGivenIsNull(){
        String src = null;
        assertEquals(new ArrayList<>(), csvHandler.read(src));
    }
    @Test
    void test(){
        List<String> actualData = csvHandler.read(csvPath);
        assertEquals(7, actualData.size());
        assertEquals("TR-47884222201,online transfer,140,USD,donation,2020-01-20,D", actualData.get(1));
    }

    @AfterEach
    void cleanUp(){
        String fileName = "test-bank-transactions.csv";
        String dest = "src\\test\\resources\\"+fileName;
        File fileCreatedForTest = new File(dest);
        if (fileCreatedForTest.exists())
            fileCreatedForTest.delete();
    }

}