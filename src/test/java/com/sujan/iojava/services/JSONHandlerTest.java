package com.sujan.iojava.services;

import com.sujan.iojava.exceptions.ParsingException;
import com.sujan.iojava.model.TransactionInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.mock.web.MockMultipartFile;

import java.io.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class JSONHandlerTest {
    @Autowired
    private JSONHandler jsonHandler;
    @Value("${sample.json.content.path}")
    private String jsonPath;

    @Test
    void shouldThrowParsingException_whenDateInTheJsonCannotBeFormatted() throws FileNotFoundException {
        String data = "[{date='20-01-2020', reference='TR-47884222201', currencyCode='USD', purpose='donation', amount='140.0'}]";
        InputStream inputStream = new ByteArrayInputStream(data.getBytes());
        assertThrows(ParsingException.class, ()->jsonHandler.read(inputStream));
    }

    @Test
    void testIfMultipartFileWithJSONFileIsReadCorrectly() throws IOException {
        FileInputStream fis = new FileInputStream(jsonPath);
        List<TransactionInfo> actualTxList = jsonHandler.read(fis);
        assertEquals(7 , actualTxList.size());
        assertEquals("TR-47884222217" , actualTxList.get(4).getTransactionId());
    }

    @AfterEach
    void cleanUp(){
        String fileName = "test-online-banking-transactions.json";
        String dest = "src\\test\\resources\\"+fileName;
        File fileCreatedForTest = new File(dest);
        if (fileCreatedForTest.exists())
            fileCreatedForTest.delete();
    }

}