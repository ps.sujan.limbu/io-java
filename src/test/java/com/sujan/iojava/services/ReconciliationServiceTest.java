package com.sujan.iojava.services;

import com.sujan.iojava.model.TransactionInfo;
import com.sujan.iojava.enums.CurrencyCode;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ReconciliationServiceTest {
    @Value("${result.directory}")
    private String resultDir;
    @Autowired
    private ReconciliationService reconciliationService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReconciliationServiceTest.class);

    @Test
    void testRecons() throws IOException {
        List<TransactionInfo> sourceData = List.of(
                new TransactionInfo("TR-47884222201","",140.00d, CurrencyCode.USD,"donation","2020-01-20","")
                ,new TransactionInfo("TR-47884222205","",60.00d, CurrencyCode.JOD,"","2020-02-03","")
                ,new TransactionInfo("TR-47884222206","",500.00d, CurrencyCode.USD,"general","2020-02-10","")
                ,new TransactionInfo("TR-47884222202","",30.00d, CurrencyCode.JOD,"donation","2020-01-22","")
                ,new TransactionInfo("TR-47884222217","",1200.00d, CurrencyCode.JOD,"salary","2020-02-14","")
                ,new TransactionInfo("TR-47884222203","",5000d, CurrencyCode.JOD,"not specified","2020-01-25","")
                ,new TransactionInfo("TR-47884222245","",420.00d, CurrencyCode.USD,"loan","2020-01-12","")
        );
        List<TransactionInfo> targetData = List.of(
                new TransactionInfo("TR-47884222201","online transfer",140d, CurrencyCode.USD,"donation","2020-01-20","D")
                ,new TransactionInfo("TR-47884222202","atm withdrwal",20.000d, CurrencyCode.JOD,"","2020-01-22","D")
                ,new TransactionInfo("TR-47884222203","counter withdrwal",5000d, CurrencyCode.JOD,"","2020-01-25","D")
                ,new TransactionInfo("TR-47884222204","salary",1200d, CurrencyCode.JOD,"general","2020-01-31","C")
                ,new TransactionInfo("TR-47884222205","atm withdrwal",60.00d, CurrencyCode.JOD,"donation","2020-02-02","D")
                ,new TransactionInfo("TR-47884222206","atm withdrwal",500.00d, CurrencyCode.USD,"salary","2020-02-10","D")
        );
        LOGGER.info(resultDir);
        String actualOutputPath = reconciliationService.executeReconciliation(sourceData, targetData);
        String expectedPath = new File(Objects.requireNonNull(resultDir)).getAbsolutePath();
        assertEquals(expectedPath, actualOutputPath);
        assertEquals(3, Objects.requireNonNull(new File(actualOutputPath).listFiles()).length);
    }

}
