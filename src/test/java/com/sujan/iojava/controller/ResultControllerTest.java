package com.sujan.iojava.controller;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
class ResultControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultControllerTest.class);

    @Test
    void test_getMatchingTransactions_new() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/api/v1/result/matching")).andReturn();
        String body = result.getResponse().getContentAsString();

        assertEquals(200, result.getResponse().getStatus());
        assertEquals("application/json", result.getResponse().getContentType());
        assertNotNull(body);
        assertTrue(body.contains("TR-47884222206"));
    }


    @Test
    void test_getMisMatchingTransactions_new() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/api/v1/result/mis-matching")).andDo(print()).andReturn();
        String body = result.getResponse().getContentAsString();

        assertEquals(200, result.getResponse().getStatus());
        assertEquals("application/json", result.getResponse().getContentType());
        assertNotNull(body);
        assertTrue(body.contains("TR-47884222205"));
    }

    @Test
    void test_getMissingTransactions_new() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/api/v1/result/missing")).andDo(print()).andReturn();
        String body = result.getResponse().getContentAsString();

        assertEquals(200, result.getResponse().getStatus());
        assertEquals("application/json", result.getResponse().getContentType());
        assertNotNull(body);
        assertTrue(body.contains("TR-47884222204"));
        assertTrue(body.contains("TR-47884222245"));
    }
}
