package com.sujan.iojava.controller;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@SpringBootTest
@AutoConfigureMockMvc
class ReconciliationControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private Environment environment;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReconciliationControllerTest.class);

    @Test
    void test_recon() throws Exception {
        Resource resource1 = new ClassPathResource(Objects.requireNonNull(environment.getProperty("sample.csv.resource.path")));
        MockMultipartFile file1 = new MockMultipartFile("sourceFile",
                resource1.getFilename(),"text/csv", resource1.getInputStream());

        Resource resource = new ClassPathResource(Objects.requireNonNull(environment.getProperty("sample.json.resource.path")));
        MockMultipartFile file2 = new MockMultipartFile("targetFile",
                resource.getFilename(),"application/json", resource.getInputStream());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v1/reconciliation/import/files")
                .file(file1)
                .file(file2))
                .andExpect(status().isOk())
                .andReturn();
        assertTrue(result.getResponse().getContentAsString().contains(Objects.requireNonNull(environment.getProperty("result.matching.transactions.name"))));
        assertTrue(result.getResponse().getContentAsString().contains(Objects.requireNonNull(environment.getProperty("result.mismatching.transactions.name"))));
        assertTrue(result.getResponse().getContentAsString().contains(Objects.requireNonNull(environment.getProperty("result.missing.transactions.name"))));
    }

}

